import React from 'react';

const AllGridIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    version="1.1"
    viewBox="0 0 512 512"
    width="19px"
    height="19px"
  >
    <g>
      <g>
        <rect y="190.67" width="160.67" height="130.67" />
      </g>
    </g>
    <g>
      <g>
        <rect x="190.67" width="130.67" height="160.67" />
      </g>
    </g>
    <g>
      <g>
        <rect x="190.67" y="190.67" width="130.67" height="130.67" />
      </g>
    </g>
    <g>
      <g>
        <rect x="190.67" y="351.33" width="130.67" height="160.67" />
      </g>
    </g>
    <g>
      <g>
        <path d="M351.333,351.333V512H467c24.813,0,45-20.187,45-45V351.333H351.333z" />
      </g>
    </g>
    <g>
      <g>
        <path d="M45,0C20.187,0,0,20.187,0,45v115.667h160.667V0H45z" />
      </g>
    </g>
    <g>
      <g>
        <path d="M0,351.333V467c0,24.813,20.187,45,45,45h115.667V351.333H0z" />
      </g>
    </g>
    <g>
      <g>
        <rect x="351.33" y="190.67" width="160.67" height="130.67" />
      </g>
    </g>
    <g>
      <g>
        <path d="M467,0H351.333v160.667H512V45C512,20.187,491.813,0,467,0z" />
      </g>
    </g>
  </svg>

);

export default AllGridIcon;
