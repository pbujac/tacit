import React from 'react';

const Logo = () => <h1 className="header__logo">ModaX</h1>;

export default Logo;
