import React from 'react';

const Nav = () => (
  <nav className="header__nav">
    <ul>
      <li>
        <a href="/">Home</a>
      </li>
      <li>
        <a href="/">Shop</a>
      </li>
      <li>
        <a href="/">Portofolio</a>
      </li>
      <li>
        <a href="/">Page</a>
      </li>
      <li>
        <a href="/">Blog</a>
      </li>
      <li>
        <a href="/">Elements</a>
      </li>
    </ul>
  </nav>
);

export default Nav;
