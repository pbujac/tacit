import React from 'react';
import Breadcrumb from './Breadcrumb';

const PageTitle = () => (
  <section className="page-title">
    <header>
      <h2 className="page-title__name">Men’s Lifestyle Clothing</h2>
    </header>
    <Breadcrumb />
  </section>
);

export default PageTitle;
