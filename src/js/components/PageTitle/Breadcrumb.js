import React from 'react';

const Breadcrumb = () => (
  <ul className="page-title__breadcrumb">
    <li className="page-title__breadcrumb__item">
      <a href="/">Home</a>
    </li>
    <li className="page-title__breadcrumb__item">
      <a href="/">Catalog</a>
    </li>
    <li className="page-title__breadcrumb__item">
      <a href="/">Menu</a>
    </li>
    <li className="page-title__breadcrumb__item">
      <a href="/">Clothing</a>
    </li>
  </ul>
);

export default Breadcrumb;
